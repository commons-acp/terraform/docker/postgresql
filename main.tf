terraform {
  required_providers {
    docker = {
      "source": "terraform-providers/docker"
    }
  }
}
variable "container_name" {
  description = "The postgres container name"
  default = "postgres_container"
}
variable "image" {
  description = "The postgres docker image"
  default = "postgres"
}
variable "postgres_env" {
  description = "The container env variables"
}
variable "postgres_port" {
  description = "The post gres exposed port"
  default = 5432
}
variable "host_postgres_path" {
  description = "The postgres host path"
  default = "/tmp/postgres"
}
variable "network_name" {
  description = "The network name"
  default = "bridge"
}
data "docker_registry_image" "postgres" {
  name = var.image
}
resource "docker_image" "postgres" {
  name          = data.docker_registry_image.postgres.name
  pull_triggers = [data.docker_registry_image.postgres.sha256_digest]
  keep_locally  = true
}
resource "docker_container" "postgres" {
  image = docker_image.postgres.latest
  name  = var.container_name

  volumes {
    host_path = var.host_postgres_path
    container_path = "/data/postgres"
    read_only = false
  }

  ports {
    internal = 5432
    external = var.postgres_port
  }

  env = var.postgres_env


  networks_advanced {
    name = var.network_name
  }



}